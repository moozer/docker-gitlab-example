# docker-gitlab-example

This is an example project to test and show if gitlab registry is functioning.

It instructs gitlab CI to

1. build the docker image and push to registry
2. test
3. promote to "latest", if on master branch and test succeeds.

Usage
--------------

1. fork/clone/copy/whatever this repo into your own instance of gitlab.
2. edit `.gitlab-ci.yml` to update the registry url and the username.


Gitlab runner config
-----------------------------

The CI pipeline is tested with a toml file like this

```
concurrent = 5
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "glrun01"
  request_concurrency = 5
  url = "https://gitlab"
  token = "SOMETOKEN"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "alpine:latest"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0

```

Debug
--------------

Docker-in-docker has certain issues. For installation and notes see [the official docs](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html)

```
error during connect: Post http://docker:2375/v1.40/auth: dial tcp: lookup docker on <dns host>:53: no such host
```

see [this](https://techoverflow.net/2021/01/12/how-to-fix-gitlab-ci-error-during-connect-post-http-docker2375-v1-40-auth-dial-tcp-lookup-docker-on-no-such-host/)

Please consider the security implications of [privileged containers](https://phoenixnap.com/kb/docker-privileged#:~:text=What%20is%20Docker%20Privileged%20Mode,App%20Arm%20and%20SELinux%20configurations).

One should use either `docker:dind` or socket binding.


```
Error response from daemon: Get "https://<registry url>/v2/": x509: certificate signed by unknown authority

```

I use a custom CA, so the root CA cert has to be installed.

The CI file introduces a CA cert from an `EXTRA_CA` as gitlab variables, and if non present, ignores it.

This is also installed in the dind service.


```
Cannot connect to the Docker daemon at tcp://docker:2375. Is the docker daemon running?
```

Docker with tls uses port 2376, not 2375. See [here](https://hub.docker.com/_/docker/#tls)

The newer versions of docker uses tls for the internal communication by default. Not doing that is a major security issue - as in root access level issue. See [gitlab blog](https://about.gitlab.com/blog/2019/07/31/docker-in-docker-with-docker-19-dot-03/) for more details.

In this project we do not use TLS. 





